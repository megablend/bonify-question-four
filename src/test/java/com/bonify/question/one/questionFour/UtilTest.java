/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bonify.question.one.questionFour;

import com.bonify.question.one.questionFour.utils.Util;
import java.util.Set;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Nexus Axis
 */
@RunWith(SpringRunner.class)
public class UtilTest {
    
    /**
     * Valid Argument Scenario
     */
    @Test
    public void whenValidArgument_returnSuccess() {
        String[] args = new String[]{"/users/Maria/info/location", "/users/Marcelo/info/birthday"};
        Set<String> staticData = Util.filterInput(args);
        String resolvedUrl = Util.resolve(staticData, args[0].split("/"));
        assertThat(staticData.isEmpty(), is(false));
        assertThat(resolvedUrl, equalTo("/users/*dynamic_part*/info/*dynamic_part*"));
    }
    
    /**
     * Invalid Argument Scenario
     */
    @Test
    public void whenInvalidArgument_returnFailed() {
        String[] args = new String[]{"/users/Maria/info/location", "/nigeria/Marcelo/silk/birthday"};
        Set<String> staticData = Util.filterInput(args);
        assertThat(staticData.isEmpty(), is(true));
    }
}
