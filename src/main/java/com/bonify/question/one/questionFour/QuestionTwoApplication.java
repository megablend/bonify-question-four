package com.bonify.question.one.questionFour;

import com.bonify.question.one.questionFour.utils.Util;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;

@Slf4j
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public class QuestionTwoApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuestionTwoApplication.class, args);
	}
        
        @Bean
        public CommandLineRunner dynamicUrlMatch() {
            return (args) -> {
                if (args.length == 1)
                    System.out.println("The URLs must be more than one :)");
                else {
                    Set<String> staticData = Util.filterInput(args);
                    
                    if (staticData.isEmpty()) 
                        System.out.println("Invalid URL pattern provided in the input");
                    else {
                        String resolvedUrl = Util.resolve(staticData, args[0].split("/"));
                        System.out.println("The URL pattern is " + resolvedUrl);
                    }
                }
            };
        }
}
