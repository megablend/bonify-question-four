/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bonify.question.one.questionFour.utils;

import java.util.HashSet;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author Nexus Axis
 */
@Slf4j
public class Util {
    public static Set<String> filterInput(String[] args) {
        Set<String> staticData = new HashSet<>();
        urls:
        for (int i = 0; i < args.length; i++) {
            String[] urlPaths = args[i].split("/");
            if (urlPaths.length <= 1) 
                System.out.println("An invalid URL was provided: " + args[i]);
            else {
                // Loop through all the records to match indexes
                for (int j = 0; j < urlPaths.length; j++) {
                    if (null == urlPaths[j] || urlPaths[j].trim().isEmpty())
                        continue;
                    // compare indexes
                    for (int k = 0; k < args.length; k++) {
                        if (i == k)
                            continue;
                        String[] urlData = args[k].split("/");
                        if (urlPaths.length != urlData.length){
                            System.out.println("Invalid URL pattern provided. The paths differ in length");
                            break urls;
                        } else {
                            for (int m = 0; m < urlData.length; m++) {
                                if (null == urlData[m] || urlData[m].trim().isEmpty())
                                    continue;
                                if (urlData[m].equals(urlPaths[j]) && m == j) 
                                    staticData.add(urlPaths[j]);
                            }
                        }
                    }
                }
            }
        }
        return staticData;
    }
    
    public static String resolve(Set<String> staticData, String[] urlPaths) {
        StringBuilder urlPattern = new StringBuilder("/");
        int count = 0;
        for (String p: urlPaths) {
            count++;
            if (null == p || p.trim().isEmpty())
                continue;
            String match = staticData.parallelStream().filter(a -> p.equals(a)).findAny().orElse(null);
            if (null != match) 
                urlPattern.append(p);
            else 
                urlPattern.append("*dynamic_part*");

            if ((count - 1) != (urlPaths.length - 1))
                urlPattern.append("/");
        }
        return urlPattern.toString();
    }
}
